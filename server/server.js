const express = require('express');
const app = express();

const port = process.env.PORT || 8080;

app.get("/", (req, res) => {
    res.send(`
      <h1>Docker + Node</h1>
      <span>Yeww</span>
    `);
  });

app.listen(port, function () {
    console.log(`Example app listening on ${port}!`);
  });